
/**
 * Escreva a descrição da classe App aqui.
 * 
 * @author (seu nome) 
 * @version (número de versão ou data)
 */
import javax.swing.JFrame;

import view.LixeiraForm;
import view.ClientForm;
import view.DesktopFrame;
import model.Database;
import view.FormClientLixeiras;
import view.SendEmailForm;
import view.Menu;


public class App
{
    public static void main(String[] args){
        
        
        
        Database db;
        ClientForm clientForm;
        LixeiraForm lixeiraForm;
        FormClientLixeiras clientLixeiraForm;
        SendEmailForm sendEmailForm;
        Menu menu;
        String dbName = "app1.db";
        String appName = "Cliente-Lixeira";
        
        db = new Database(dbName);
        
        DesktopFrame desktop = new DesktopFrame(db,appName);
        desktop.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        desktop.setSize(800,1000);
        desktop.setLocationRelativeTo(null);
        desktop.setVisible(true);
    }
}

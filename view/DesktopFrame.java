
package view;


/**
 * Escreva a descrição da classe DesktopFrame aqui.
 * 
 * @author (seu nome) 
 * @version (número de versão ou data)
 */
import model.Database;
import view.ClientForm;
import view.LixeiraForm;
import view.FormClientLixeiras;
import view.util.FormUtils;
import view.SendEmailForm;

import model.Database;
import javax.swing.JDesktopPane;
import java.awt.BorderLayout;
import java.awt.Dialog;
import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import javax.swing.JDialog;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.Color;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

public class DesktopFrame extends JFrame implements ActionListener
{
    private final JDesktopPane desktop;
    private String appNome;
    private Database database;
    
    private JMenu menuEntidades;
    JMenuItem menuClients;
    JMenuItem menuLixeiras;
    JMenuItem menuClientLixeiras;
    JMenuItem menuSendEmail;
    
    JFrame tela = new JFrame();
    
    public DesktopFrame(Database db, String n){
        super(n);
        appNome = n;
        database = db;
        
        JMenuBar barra = new JMenuBar();
        menuEntidades = new JMenu("Entidades");
        menuClients = new JMenuItem("Clientes");
        menuLixeiras = new JMenuItem("Lixeiras");
        menuClientLixeiras = new JMenuItem("Cliente-Lixeira");
        menuSendEmail = new JMenuItem("Enviar Email");
        
        menuEntidades.add(menuClients);
        menuEntidades.add(menuLixeiras);
        menuEntidades.add(menuClientLixeiras);
        menuEntidades.add(menuSendEmail);
        barra.add(menuEntidades);
        setJMenuBar(barra);
        
        menuClients.addActionListener(this);
        menuLixeiras.addActionListener(this);
        menuClientLixeiras.addActionListener(this);
        menuSendEmail.addActionListener(this);
        
        desktop = new JDesktopPane();
        add(desktop);
        
        FormUtils.centerForm(this);
    }
    
    public void actionPerformed(ActionEvent e){
        if(e.getSource().equals(menuClients)){
            menuClients_Click();
        }
       
        if(e.getSource().equals(menuLixeiras)){
            menuLixeiras_Click();
        }
        
        if(e.getSource().equals(menuClientLixeiras)){
            menuClientLixeiras_Click();
        }
        
        if(e.getSource().equals(menuSendEmail)){
            menuSendEmail_Click();
        }
        
       
    }
    
    private void menuClients_Click(){
        this.setEnabled(false);
        ClientForm formClient = new ClientForm(this,database,appNome);
        formClient.criarExibirForm();
    }
    
    private void menuLixeiras_Click(){
        this.setEnabled(false);
        LixeiraForm formLixeira = new LixeiraForm(this,database,appNome);
        formLixeira.criarExibirForm();
    }
    
    private void menuClientLixeiras_Click(){
        this.setEnabled(false);
        FormClientLixeiras formClientLixeiras = new FormClientLixeiras(this,database,appNome);
        formClientLixeiras.criarExibirForm();
    }  
    
    private void menuSendEmail_Click(){
        this.setEnabled(false);
        SendEmailForm sendEmailForm = new SendEmailForm(this,database,appNome);
        sendEmailForm.criarExibirForm();
    }  
    
    
}
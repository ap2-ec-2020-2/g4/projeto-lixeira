
package view;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.GridLayout;
import java.awt.Dimension;
import java.util.ArrayList;
import java.awt.FlowLayout;
import javax.swing.border.TitledBorder;
import javax.swing.border.LineBorder;
import java.awt.Color;
import javax.swing.table.DefaultTableModel;
import java.awt.*;

import model.entity.ClientLixeira;
import model.entity.Lixeira;
import model.entity.Client;
import model.Database;
import java.sql.*;

import controller.ClientLixeiraController;
import controller.ClientController;
import controller.LixeiraController;

import view.util.CrudToolBar;
import view.util.FormUtils;
import view.FormEntidade;

/**
 * Escreva a descrição da classe ClientLixeiraForm aqui.
 * 
 * @author (seu nome) 
 * @version (número de versão ou data)
 */
public class FormClientLixeiras extends FormEntidade
{
    private JFrame frame;
    private JFrame formPai;
    
    private ClientLixeiraController controller;
    private ClientController controllerClient;
    private LixeiraController controllerLixeira;
    private Database database;
    private List<ClientLixeira> clientLixeiraList;
    private List<Client> clientList;
    private List<Lixeira> lixeiraList;
    private List<Lixeira> lixeirasSelecionadas;
    
    private JButton botaoRelacionar;
    private JButton botaoPrevious;
    private JButton botaoNext;
    
    private JPanel painelPrincipal;
    //private JPanel subPainel_1;
    private JPanel subPainel_1;
    private JPanel subPainel_2;
    private JPanel subPainel_3;
    
    private JComboBox<Client> comboClients;
    
    private JTextField tfIdClient;
    private JTextField tfInstitutionClient;
    private JTextField tfFoneClient;
    private JTextField tfEmailClient;
    private JTextField tfCnpjClient;
    
    private JTable tabela;
    private JScrollPane barraRolagem;
    private DefaultTableModel modeloTabela;
    
    private JButton botaoReturn;
    
    public FormClientLixeiras(JFrame form, Database db, String n){
        formPai = form;
        this.database = db;
        this.controller = new ClientLixeiraController(database);
        this.controllerClient = new ClientController(database);
        this.controllerLixeira = new LixeiraController(database);
        setAppNome(n);
    }
    
    public void criarExibirForm(){
        lerClients();
        
        frame = new JFrame(getAppNome());
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        Font font = new Font("Berlin Sans", Font.BOLD, 18);
        Font font1 = new Font("Berlin Sans", Font.PLAIN, 18);
        
        painelPrincipal = new JPanel();
        painelPrincipal.setLayout(new GridLayout(3,1));
        
        subPainel_1 = new JPanel();
        subPainel_1.setBackground(new java.awt.Color(52, 73, 94));
        subPainel_1.setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = new Insets(3,3,3,3);
        
        botaoRelacionar = new JButton("RELACIONAR");
        botaoRelacionar.setFont(font);
        gbc.gridx = 0;
        gbc.gridy = 0;
        botaoRelacionar.setIcon(new ImageIcon("link.png"));
        botaoRelacionar.setBackground(new java.awt.Color(218, 223, 225));
        botaoRelacionar.setPreferredSize(new Dimension(200, 45));
        botaoRelacionar.addActionListener(this);
        subPainel_1.add(botaoRelacionar);
        botaoPrevious = new JButton("PROXIMO");
        botaoPrevious.setFont(font);
        gbc.gridx = 1;
        gbc.gridy = 0;
        botaoPrevious.setIcon(new ImageIcon("next.png"));
        botaoPrevious.setBackground(new java.awt.Color(218, 223, 225));
        botaoNext = new JButton("ANTERIOR");
        botaoNext.setFont(font);
        gbc.gridx = 2;
        gbc.gridy = 0;
        botaoNext.setIcon(new ImageIcon("previous.png"));
        botaoNext.setBackground(new java.awt.Color(218, 223, 225));
        botaoPrevious.setPreferredSize(new Dimension(200, 45));
        botaoNext.setPreferredSize(new Dimension(200, 45));
        botaoPrevious.addActionListener(this);
        botaoNext.addActionListener(this);
        subPainel_1.add(botaoNext);
        subPainel_1.add(botaoPrevious);
        painelPrincipal.add(subPainel_1);
        
        //Clientes
        JLabel label8 = new JLabel("CLIENTES:");
        label8.setFont(font);
        label8.setForeground(Color.white);
        label8.setIcon(new ImageIcon("cliente.png"));
        gbc.gridx = 0;
        gbc.gridy = 2;
        gbc.gridwidth = 7;
        subPainel_1.add(label8, gbc);
        
        JLabel label9 = new JLabel(" ");
        gbc.gridx = 0;
        gbc.gridy = 8;
        gbc.gridwidth = 7;
        subPainel_1.add(label9, gbc);
        
        //ID
        gbc.gridx = 0;
        gbc.gridy = 3;
        gbc.gridwidth = 1;
        JLabel label1 = new JLabel("ID:");
        label1.setFont(font);
        label1.setForeground(Color.white);
        label1.setIcon(new ImageIcon("id.png"));
        subPainel_1.add(label1, gbc);
        tfIdClient = new JTextField();
        tfIdClient.setFont(font);
        tfIdClient.setPreferredSize(new Dimension(70,25));
        tfIdClient.setBackground(new Color(242, 241, 239));
        tfIdClient.setEnabled(false);
        gbc.gridx = 1;
        gbc.gridy = 3;
        gbc.gridwidth = 2;
        subPainel_1.add(tfIdClient,gbc);
        
        
        //INSTITUTION
        gbc.gridx = 0;
        gbc.gridy = 4;
        gbc.gridwidth = 1;
        JLabel label2 = new JLabel("INSTITUIÇÃO:");
        label2.setFont(font);
        label2.setForeground(Color.white);
        label2.setIcon(new ImageIcon("institution.png"));
        subPainel_1.add(label2, gbc);
        tfInstitutionClient = new JTextField();
        tfInstitutionClient.setFont(font);
        tfInstitutionClient.setPreferredSize(new Dimension(400,25));
        tfInstitutionClient.setBackground(new Color(242, 241, 239));
        tfInstitutionClient.setEnabled(false);
        gbc.gridx = 1;
        gbc.gridy = 4;
        gbc.gridwidth = 2;
        subPainel_1.add(tfInstitutionClient,gbc);
        
        //CNPJ
        gbc.gridx = 0;
        gbc.gridy = 5;
        gbc.gridwidth = 1;
        JLabel label5 = new JLabel("CNPJ:");
        label5.setFont(font);
        label5.setForeground(Color.white);
        label5.setIcon(new ImageIcon("cnpj.png"));
        subPainel_1.add(label5, gbc);
        tfCnpjClient = new JTextField();
        tfCnpjClient.setFont(font);
        tfCnpjClient.setPreferredSize(new Dimension(400,25));
        tfCnpjClient.setBackground(new Color(242, 241, 239));
        tfCnpjClient.setEnabled(false);
        gbc.gridx = 1;
        gbc.gridy = 5;
        gbc.gridwidth = 2;
        subPainel_1.add(tfCnpjClient,gbc);
        
        //FONE
        gbc.gridx = 0;
        gbc.gridy = 6;
        gbc.gridwidth = 1;
        JLabel label3 = new JLabel("FONE:");
        label3.setFont(font);
        label3.setForeground(Color.white);
        label3.setIcon(new ImageIcon("fone.png"));
        subPainel_1.add(label3, gbc);
        tfFoneClient = new JTextField();
        tfFoneClient.setFont(font);
        tfFoneClient.setPreferredSize(new Dimension(400,25));
        tfFoneClient.setBackground(new Color(242, 241, 239));
        tfFoneClient.setEnabled(false);
        gbc.gridx = 1;
        gbc.gridy = 6;
        gbc.gridwidth = 2;
        subPainel_1.add(tfFoneClient,gbc);
        
        //EMAIL
        gbc.gridx = 0;
        gbc.gridy = 7;
        gbc.gridwidth = 1;
        JLabel label4 = new JLabel("EMAIL:");
        label4.setFont(font);
        label4.setForeground(Color.white);
        label4.setIcon(new ImageIcon("email.png"));
        subPainel_1.add(label4, gbc);
        tfEmailClient = new JTextField();
        tfEmailClient.setFont(font);
        tfEmailClient.setPreferredSize(new Dimension(400,25));
        tfEmailClient.setBackground(new Color(242, 241, 239));
        tfEmailClient.setEnabled(false);
        gbc.gridx = 1;
        gbc.gridy = 7;
        gbc.gridwidth = 2;
        subPainel_1.add(tfEmailClient,gbc);
        
        JLabel label10 = new JLabel("LIXEIRAS:");
        label10.setFont(font);
        label10.setForeground(Color.white);
        label10.setIcon(new ImageIcon("lixo.png"));
        gbc.gridx = 0;
        gbc.gridy = 9;
        gbc.gridwidth = 7;
        subPainel_1.add(label10, gbc);
        
        painelPrincipal.add(subPainel_1);
        
        //Lixeiras
        subPainel_2 = new JPanel();
        subPainel_2.setBackground(new java.awt.Color(52, 73, 94));
        //subPainel_2.setBorder(new TitledBorder(new LineBorder
        //(Color.BLACK), "Lixeiras:"));
        subPainel_2.setLayout(new GridLayout(1,1));
        
        String [] colunasTabela = {"Localização"};

        tabela = new JTable();
        tabela.setBackground(new Color(242, 241, 239));
        tabela.setGridColor(Color.BLACK);
        tabela.setFont(font1);
        modeloTabela = new DefaultTableModel();
        modeloTabela.setColumnIdentifiers(colunasTabela);
        tabela.setModel(modeloTabela);
        barraRolagem = new JScrollPane(tabela);
        subPainel_2.add(barraRolagem);
        UIManager.put("TableHeader.font",new Font("Berlin Sans", Font.BOLD, 18) );
        painelPrincipal.add(subPainel_2);
        
        subPainel_3 = new JPanel();
        subPainel_3.setBackground(new java.awt.Color(52, 73, 94));
        subPainel_3.setLayout(new FlowLayout(FlowLayout.RIGHT));
        botaoReturn = new JButton("RETORNAR");
        botaoReturn.setIcon(new ImageIcon("return.png"));
        botaoReturn.setPreferredSize(new Dimension(140, 50));
        botaoReturn.setBackground(new java.awt.Color(218, 223, 225));
        subPainel_3.add(botaoReturn);
        botaoReturn.addActionListener(this);
        
        painelPrincipal.add(subPainel_3);
        
        painelPrincipal.setPreferredSize(new Dimension(1000,1000));
        frame.add(painelPrincipal);
        frame.pack();
        FormUtils.centerForm(frame);        
        frame.setVisible(true);

        exibirEntidade(getNumEntidadeCorrente());
    }    
        
    public void actionPerformed(ActionEvent e){
        if(e.getSource().equals(botaoRelacionar)){
            botaoRelacionar_Click();
        }
        
        if(e.getSource().equals(botaoPrevious)){
            botaoPrevious_Click();
        }
        
        if(e.getSource().equals(botaoNext)){
            botaoNext_Click();
        }
        
        if(e.getSource().equals(botaoReturn)){
            botaoReturn_Click();
        }
    }
    
    private void preencherTabela(List<Lixeira> lixeiras){
        while(modeloTabela.getRowCount()>0){
            modeloTabela.removeRow(0);
        }
        
        for(Lixeira l: lixeiras){
            Object [] linha = new Object [] {l.getLoc()};
            modeloTabela.addRow(linha);
        }
    }
    
    public Client getClientCorrente(){
        Client c = clientList.get(getNumEntidadeCorrente());
        
        return c;
    }
    
    public void salvarEntidade(){
    }
    
    public void alterarEntidade(){
    }
    
    public void deletarEntidade(){
    }
    
    public void lerClients(){
        clientList = controllerClient.readAll();
        
        if(clientList.isEmpty()){
            setNumEntidadeCorrente(-1);
        }
        else{
            setNumEntidadeCorrente(0);
        }
    }
    
    public void lerEntidades(){
    }
    
    public void exibirEntidade(int index){
        Client client;
        
        if(index == -1){
            FormUtils.clearTextFields(this,subPainel_1);
        }
        else{
            client = clientList.get(index);
            tfIdClient.setText(Integer.toString(client.getId()));
            tfInstitutionClient.setText(client.getInstitution());
            tfFoneClient.setText(client.getFone());
            tfCnpjClient.setText(client.getCnpj());
            tfEmailClient.setText(client.getEmail());
            
            preencherTabela(lerLixeiras(client.getId()));
        }
    }
        
    private List<Lixeira> lerLixeiras(int idClient){
        List<Lixeira> listaLixeiras = new ArrayList<Lixeira>();
        
        listaLixeiras = controller.readLixeiraDeClient(idClient);
        
        return listaLixeiras;
    }
    
    public int totalEntidades(){
        return clientList.size();
    }
    
    public JPanel getSubPainelCampos(){
        return subPainel_1;
    }
    
    public void setLixeirasSelecionadas(List<Lixeira> lista){
        int cod_lixeira, cod_client;
        Client client = clientList.get(getNumEntidadeCorrente());
        
        lixeirasSelecionadas = lista;
        
        for(Lixeira l: lista){
            cod_client = client.getId();
            cod_lixeira = l.getId();
            
            ClientLixeira cl = new ClientLixeira();
            cl.setClient(client);
            cl.setLixeira(l);
            
            controller.create(cl);
        }
        
        preencherTabela(lerLixeiras(getClientCorrente().getId()));
    }
    
    private void botaoRelacionar_Click(){
        DialogRelacionarLixeiraClient dialogRelacionar = new DialogRelacionarLixeiraClient(this,database,getAppNome());
        dialogRelacionar.criarExibirForm();
    }
    
    private void botaoNext_Click(){
        int numEntidadeCorrente = getNumEntidadeCorrente();
        
        if((numEntidadeCorrente-1) >= 0){
            numEntidadeCorrente--;
            setNumEntidadeCorrente(numEntidadeCorrente);
            exibirEntidade(numEntidadeCorrente);
        }
    }
    
    private void botaoPrevious_Click(){
        int numEntidadeCorrente = getNumEntidadeCorrente();
        
        if((numEntidadeCorrente+1) < totalEntidades()){
            numEntidadeCorrente++;
            setNumEntidadeCorrente(numEntidadeCorrente);
            exibirEntidade(numEntidadeCorrente);
        }
    }
    
    private void botaoReturn_Click(){
        frame.setVisible(false);
        formPai.setEnabled(true);
        frame.dispose();
    }
}
    


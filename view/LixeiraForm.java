package view;

/**
 * Escreva a descrição da classe LixeiraForm aqui.
 * 
 * @author (seu nome) 
 * @version (número de versão ou data)
 */


import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.border.TitledBorder;
import javax.swing.border.LineBorder;
import java.io.IOException;
import java.net.URISyntaxException;

import controller.LixeiraController;
import model.Database;
import model.entity.Lixeira;

import view.util.CrudToolBar;
import view.util.FormUtils;
import view.FormEntidade;
import view.Menu;

public class LixeiraForm extends FormEntidade
{

    private JFrame frame;
    private JFrame formPai;
    private LixeiraController controller;
    private Database database;
    private List<Lixeira> lixeiraList;
    
    private CrudToolBar crudToolBar;
    
    private JPanel painelPrincipal;
    private JPanel subPanel_1;
    private JPanel subPanel_2;
    
    private JTextField tfId;
    private JTextField tfLoc;
    private JButton bReturn;
    private JButton bMaps;
    private JButton bSituação;
    
    public LixeiraForm(JFrame form, Database database, String n){
        formPai = form;
        this.controller = new LixeiraController(database);
        setAppNome(n);
    } 
    

    public void criarExibirForm(){
        frame = new JFrame(getAppNome());
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        Font font = new Font("Berlin Sans", Font.BOLD, 17);

        painelPrincipal = new JPanel();
        painelPrincipal.setLayout(new GridLayout(3,1));
        crudToolBar = new CrudToolBar(this);
        crudToolBar.setBackground(new java.awt.Color(52, 73, 94));
        painelPrincipal.add(crudToolBar);
        
        
        subPanel_1 = new JPanel();
        
        subPanel_1.setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = new Insets(3,3,3,3);
        
        JLabel label0 = new JLabel("CADASTRO LIXEIRAS");
        label0.setIcon(new ImageIcon("lixo.png"));
        label0.setFont(font);
        label0.setForeground(Color.white);
        gbc.gridx = 0;
        gbc.gridy = 0;
        subPanel_1.add(label0, gbc);
        
        JLabel label1 = new JLabel(" ");
        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.gridwidth = 7;
        subPanel_1.add(label1, gbc);
        
        //ID
        gbc.gridx = 0;
        gbc.gridy = 2;
        gbc.gridwidth = 1;
        JLabel label2 = new JLabel("ID:");
        label2.setFont(font);
        label2.setForeground(Color.white);
        label2.setIcon(new ImageIcon("id.png"));
        subPanel_1.add(label2, gbc);
        
        tfId = new JTextField();
        tfId.setBackground(new Color(242, 241, 239));
        tfId.setFont(font);
        tfId.setPreferredSize(new Dimension(60,25));
        tfId.setEnabled(false);
        gbc.gridx = 1;
        gbc.gridy = 2;
        gbc.gridwidth = 1;
        subPanel_1.add(tfId, gbc);
        
        JLabel label4 = new JLabel(" ");
        gbc.gridx = 0;
        gbc.gridy = 3;
        gbc.gridwidth = 7;
        subPanel_1.add(label4, gbc);
        
        //Loc
        gbc.gridx = 0;
        gbc.gridy = 4;
        gbc.gridwidth = 1;
        JLabel label3 = new JLabel("LOCALIZAÇÃO:");
        label3.setFont(font);
        label3.setForeground(Color.white);
        label3.setIcon(new ImageIcon("localization.png"));
        subPanel_1.add(label3, gbc);
        
        tfLoc = new JTextField();
        tfLoc.setBackground(new Color(242, 241, 239));
        tfLoc.setFont(font);
        tfLoc.setPreferredSize(new Dimension(400,25));
        gbc.gridx = 1;
        gbc.gridy = 4;
        gbc.gridwidth = 7;
        subPanel_1.add(tfLoc, gbc);
        
        painelPrincipal.add(subPanel_1);
        
        subPanel_2 = new JPanel();
        subPanel_2.setLayout(new FlowLayout(FlowLayout.RIGHT,3,3));
        bReturn = new JButton("RETORNAR");
        bReturn.setIcon(new ImageIcon("return.png"));
        bReturn.setPreferredSize(new Dimension(160, 50));
        bReturn.setBackground(new java.awt.Color(218, 223, 225));
        subPanel_2.add(bReturn);
        bReturn.addActionListener(this);
        
        bMaps = new JButton("IR PARA O MAPS");
        bMaps.setIcon(new ImageIcon("maps.png"));
        bMaps.setPreferredSize(new Dimension(170, 50));
        bMaps.setBackground(new java.awt.Color(218, 223, 225));
        subPanel_2.add(bMaps);
        bMaps.addActionListener(this);
        
        bSituação = new JButton("STATUS");
        bSituação.setIcon(new ImageIcon("status.png"));
        bSituação.setPreferredSize(new Dimension(170, 50));
        bSituação.setBackground(new java.awt.Color(218, 223, 225));
        subPanel_2.add(bSituação);
        bSituação.addActionListener(this);
        
        subPanel_1.setBackground(new java.awt.Color(52, 73, 94));
        subPanel_2.setBackground(new java.awt.Color(52, 73, 94));
        
        painelPrincipal.add(subPanel_2);
        
        frame.add(painelPrincipal);
        
        frame.pack();
        FormUtils.centerForm(frame);
        frame.setVisible(true);
        
        lerEntidades();
        exibirEntidade(getNumEntidadeCorrente());
    }
    
    public void actionPerformed(ActionEvent e){
        if(e.getSource().equals(bReturn)){
            bReturn_Click();
        }
        
        if(e.getSource().equals(bMaps)){
            bMaps_Click();
        }
        
        if(e.getSource().equals(bSituação)){
            bSituação_Click();
        }
    }
    
    private void bReturn_Click(){
        frame.setVisible(false);
        formPai.setEnabled(true);
        frame.dispose();
    }
    
    private void bSituação_Click(){
        try{
            java.awt.Desktop.getDesktop().browse( new java.net.URI( "https://thingspeak.com/channels/1412738" ) );
        }catch(IOException e){
            System.err.println(e);
        }catch(URISyntaxException e){
            System.err.println(e);
        }
    }
    
    private void bMaps_Click(){
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Menu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Menu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Menu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Menu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Menu().setVisible(true);
            }
        });
    }
    
    public Lixeira getEntidadeCorrente(){
        Lixeira lixeira = new Lixeira();
        int id;
        
        if(tfId.getText().isEmpty() == false){
            id = Integer.parseInt(tfId.getText());
            lixeira.setId(id);
        }
        
        if(tfLoc.getText().isEmpty() == false){
            lixeira.setLoc(tfLoc.getText());
        }
        return lixeira;   
    }
    
    public void salvarEntidade(){
        Lixeira lixeira;
        
        lixeira = getEntidadeCorrente();
        Lixeira lixeiraCreate = controller.create(lixeira);
    }
    
    public void alterarEntidade(){
        Lixeira lixeira = new Lixeira();
        
        lixeira = getEntidadeCorrente();
        
        controller.update(lixeira);
        
        lerEntidades();
        exibirEntidade(getNumEntidadeCorrente());
    }
    
    public void deletarEntidade(){
        Lixeira lixeira = new Lixeira();
        
        lixeira = getEntidadeCorrente();
        
        controller.delete(lixeira);
        
        lerEntidades();
        exibirEntidade(getNumEntidadeCorrente());
    }
    
    public void lerEntidades(){
        lixeiraList = controller.readAll();
        
        if(lixeiraList.isEmpty()){
            setNumEntidadeCorrente(-1);
        }else{
            setNumEntidadeCorrente(0);
        }
    }
    
    public void exibirEntidade(int index){
        Lixeira lixeira;
        
        if(index == -1){
            FormUtils.clearTextFields(this,subPanel_1);
        }
        else{
            lixeira = lixeiraList.get(index);
            tfId.setText(Integer.toString(lixeira.getId()));
            tfLoc.setText(lixeira.getLoc());
        }
    }
    
    public int totalEntidades(){
        return lixeiraList.size();
    }
       
    public JPanel getSubPainelCampos(){
        return subPanel_1;
    }

}
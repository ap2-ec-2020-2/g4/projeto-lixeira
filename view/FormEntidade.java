package view;


/**
 * Classe abstrata FormEntidade - escreva a descrição da classe aqui
 * 
 * @author (seu nome aqui)
 * @version (versão ou data)
 */

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import model.entity.Client;

public abstract class FormEntidade extends JFrame implements ActionListener
{
    private String appNome;
    private int numEntidadeCorrente;
    
    public abstract void salvarEntidade();
    
    public abstract void deletarEntidade();
    
    public abstract void alterarEntidade();
    
    public abstract void lerEntidades();
    
    public abstract void exibirEntidade(int index);
    
    public abstract int totalEntidades();
    
    public abstract JPanel getSubPainelCampos();
    
  

    //Start GetterSetterExtension Source Code
    /**GET Method Propertie appNome*/
    public String getAppNome(){
        return this.appNome;
    }//end method getAppNome

    /**SET Method Propertie appNome*/
    public void setAppNome(String appNome){
        this.appNome = appNome;
    }//end method setAppNome

    /**GET Method Propertie numEntidadeCorrente*/
    public int getNumEntidadeCorrente(){
        return this.numEntidadeCorrente;
    }//end method getNumEntidadeCorrente

    /**SET Method Propertie numEntidadeCorrente*/
    public void setNumEntidadeCorrente(int numEntidadeCorrente){
        this.numEntidadeCorrente = numEntidadeCorrente;
    }//end method setNumEntidadeCorrente

    //End GetterSetterExtension Source Code
//!
}

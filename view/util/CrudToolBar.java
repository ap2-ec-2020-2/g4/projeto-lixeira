package view.util;


/**
 * Escreva a descrição da classe CrudToolBar aqui.
 * 
 * @author (seu nome) 
 * @version (número de versão ou data)
 */

import model.entity.*;
import view.util.FormUtils;
import view.FormEntidade;

import javax.swing.*;
import java.awt.*;
import java.util.List;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class CrudToolBar extends JPanel implements ActionListener
{
    private static int ALTURA = 55;
    private static int LARGURA = 70;
    private FormEntidade form;
    private GridBagLayout layout;
    private GridBagConstraints gbc;
    
    private JButton bCancel;
    private JButton bCreate;
    private JButton bDelete;
    private JButton bExit;
    private JButton bNext;
    private JButton bOK;
    private JButton bPrevious;
    private JButton bUpdate;
    
    private Dimension tamanhoBotao;
    
    public CrudToolBar(FormEntidade f){
        this.form = f;
        this.layout = new GridBagLayout();
        this.setLayout(layout);
        this.gbc = new GridBagConstraints();
        this.gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = new Insets(2,2,2,2);
        this.tamanhoBotao = new Dimension(CrudToolBar.LARGURA,CrudToolBar.ALTURA);
        
        JToolBar toolBar = new JToolBar("Still draggable");
        toolBar.setFloatable(false);
        toolBar.setRollover(true);
        adicionarBotoes();
        associarEventos();
    }
    
    private void adicionarBotoes(){
        bCreate = criarBotao(0,0,"");
        bCreate.setIcon(new ImageIcon("new.png"));
        bCreate.setBackground(new java.awt.Color(218, 223, 225));
        
        bPrevious = criarBotao(1,0,"");
        bPrevious.setIcon(new ImageIcon("previous.png"));
        bPrevious.setBackground(new java.awt.Color(218, 223, 225));
        
        bNext = criarBotao(2,0,"");
        bNext.setIcon(new ImageIcon("next.png"));
        bNext.setBackground(new java.awt.Color(218, 223, 225));
        
        bUpdate = criarBotao(3,0,"");
        bUpdate.setIcon(new ImageIcon("edit.png"));
        bUpdate.setBackground(new java.awt.Color(218, 223, 225));
        
        bDelete = criarBotao(4,0,"");
        bDelete.setIcon(new ImageIcon("delete.png"));
        bDelete.setBackground(new java.awt.Color(218, 223, 225));
        
        bOK = criarBotao(5,0,"");
        bOK.setIcon(new ImageIcon("emblem-default.png"));
        bOK.setBackground(new java.awt.Color(218, 223, 225));
        bOK.setEnabled(false);
        
        bCancel = criarBotao(6,0,"");
        bCancel.setIcon(new ImageIcon("cancecl.png"));
        bCancel.setBackground(new java.awt.Color(218, 223, 225));
        bCancel.setEnabled(false);
    }
    
    private JButton criarBotao(int x, int y, String rotulo){
        this.gbc.gridx = x;
        this.gbc.gridy = y;
        JButton botao = new JButton(rotulo);
        botao.setPreferredSize(tamanhoBotao);
        this.add(botao, this.gbc);
        
        return botao;
    }
    
    private void associarEventos(){
        Component components [] = this.getComponents();
        
        for(Component c : components){
            if(c instanceof JButton){
                ((JButton) c).addActionListener(this);
            }
        }
    }
    
    public void actionPerformed(ActionEvent e){
        if(e.getSource().equals(bCreate)){
            bCreate_Click();
        }

        if(e.getSource().equals(bCancel))
        {
            bCancel_Click();
        }        

        if(e.getSource().equals(bOK))
        {
            bOK_Click();
        }

        if(e.getSource().equals(bDelete))
        {
            int option;

            option = JOptionPane.showConfirmDialog(form,
                "Voce deseja excluir o cadastro atual?",
                form.getAppNome(),
                JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE);

            if (option == JOptionPane.YES_OPTION)
            {           
                bDelete_Click();
            }            
        }
        
        if(e.getSource().equals(bUpdate))
        {
            int option;

            option = JOptionPane.showConfirmDialog(form,
                "Voce deseja alterar os dados atuais?",
                form.getAppNome(),
                JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE);

            if (option == JOptionPane.YES_OPTION)
            {           
                bUpdate_Click();
            }
        }
        
        if(e.getSource().equals(bNext))
        {
            bNext_Click();
        }
        
        if(e.getSource().equals(bPrevious))
        {
            bPrevious_Click();
        }
    }
       
    private void bOK_Click(){
        bOK.setEnabled(false);
        bCancel.setEnabled(false);
        bPrevious.setEnabled(true);
        bNext.setEnabled(true);
        bUpdate.setEnabled(true);
        bDelete.setEnabled(true);
        bCreate.setEnabled(true);

        form.salvarEntidade();
        form.lerEntidades();
        form.exibirEntidade(form.getNumEntidadeCorrente());
    } 
    
    private void bCancel_Click(){
        bOK.setEnabled(false);
        bCancel.setEnabled(false);
        bPrevious.setEnabled(true);
        bNext.setEnabled(true);
        bUpdate.setEnabled(true);
        bDelete.setEnabled(true);
        bCreate.setEnabled(true);

        form.lerEntidades();
        form.exibirEntidade(form.getNumEntidadeCorrente());
    } 
        
    private void bCreate_Click(){
        bOK.setEnabled(true);
        bCancel.setEnabled(true);
        bNext.setEnabled(false);
        bPrevious.setEnabled(false);
        bUpdate.setEnabled(false);
        bDelete.setEnabled(false);
        bCreate.setEnabled(false);
        
        FormUtils.clearTextFields(form, form.getSubPainelCampos());
    }
    
    private void bPrevious_Click(){
        int numEntidadeCorrente = form.getNumEntidadeCorrente();
        
        if((numEntidadeCorrente-1) >= 0){
            numEntidadeCorrente--;
            form.setNumEntidadeCorrente(numEntidadeCorrente);
            form.exibirEntidade(numEntidadeCorrente);
        }
    }
    
    private void bNext_Click(){
        int numEntidadeCorrente = form.getNumEntidadeCorrente();
        
        if((numEntidadeCorrente+1) < form.totalEntidades()){
            numEntidadeCorrente++;
            form.setNumEntidadeCorrente(numEntidadeCorrente);
            form.exibirEntidade(numEntidadeCorrente);
        }
    }
    
    private void bUpdate_Click(){
        form.alterarEntidade();
    }
    
    private void bDelete_Click(){
        form.deletarEntidade();
    }     
}

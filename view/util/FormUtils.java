package view.util;


/**
 * Escreva a descrição da classe FormUtils aqui.
 * 
 * @author (seu nome) 
 * @version (número de versão ou data)
 */

import javax.swing.*;
import java.awt.*;

public class FormUtils
{
    public static void clearTextFields(JFrame frame, JPanel panel){
       Container container = frame.getContentPane();
       
       Component components [] = panel.getComponents();
       
       for(Component c : components){
           if(c instanceof JTextField){
               ((JTextField) c).setText("");
            }
        }
    }
    
    public static void centerForm(JFrame frame){
        Dimension windowSize = frame.getSize();
        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        Point centerPoint = ge.getCenterPoint();
        
        int dx = centerPoint.x - windowSize.width/2;
        int dy = centerPoint.y - windowSize.height/2;
        
        frame.setLocation(dx, dy);
    }
}

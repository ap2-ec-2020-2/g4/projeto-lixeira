package view;


/**
 * Escreva a descrição da classe ClientForm aqui.
 * 
 * @author (seu nome) 
 * @version (número de versão ou data)
 */
import javax.swing.*;
import java.awt.*;
import java.util.List;
import javax.swing.border.TitledBorder;
import javax.swing.border.LineBorder;
import java.awt.event.ActionEvent;

import model.entity.Client;
import model.Database;
import controller.ClientController;
import view.util.FormUtils;
import view.util.CrudToolBar;
import view.FormEntidade;

public class ClientForm extends FormEntidade
{
    private JFrame frame;
    private JFrame formPai;
    private ClientController controller;
    private Database database;
    private List<Client> clientList;
    
    private CrudToolBar crudToolBar;
    
    private JPanel painelPrincipal;
    private JPanel subPanel_1;
    private JPanel subPanel_2;
    
    private JTextField tfId;
    private JTextField tfInstitution;
    private JTextField tfCnpj;
    private JTextField tfFone;
    private JTextField tfEmail;
    
    private JButton bReturn;
    
    public ClientForm(JFrame form, Database database, String n){
        formPai = form;
        this.controller = new ClientController(database);
        setAppNome(n);
    }
    
    public void criarExibirForm(){
        frame = new JFrame(getAppNome());
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        Font font = new Font("Berlin Sans", Font.BOLD, 17);
        
        painelPrincipal = new JPanel();
        painelPrincipal.setLayout(new GridLayout(3,1));
        crudToolBar = new CrudToolBar(this);
        crudToolBar.setBackground(new java.awt.Color(52, 73, 94));
        painelPrincipal.add(crudToolBar);
        
        subPanel_1 = new JPanel();
        
        subPanel_1.setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = new Insets(3,3,3,3);
        
        JLabel label0 = new JLabel("CADASTRO CLIENTES");
        label0.setIcon(new ImageIcon("cliente.png"));
        label0.setFont(font);
        label0.setForeground(Color.white);
        gbc.gridx = 0;
        gbc.gridy = 0;
        subPanel_1.add(label0, gbc);
        
        JLabel label7 = new JLabel(" ");
        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.gridwidth = 7;
        subPanel_1.add(label7, gbc);
        
        //ID
        gbc.gridx = 0;
        gbc.gridy = 2;
        gbc.gridwidth = 1;
        JLabel label1 = new JLabel("ID:");
        label1.setFont(font);
        label1.setForeground(Color.white);
        label1.setIcon(new ImageIcon("id.png"));
        subPanel_1.add(label1, gbc);
        
        tfId = new JTextField();
        tfId.setBackground(new Color(242, 241, 239));
        tfId.setFont(font);
        tfId.setPreferredSize(new Dimension(70,25));
        tfId.setEnabled(false);
        gbc.gridx = 1;
        gbc.gridy = 2;
        gbc.gridwidth = 1;
        subPanel_1.add(tfId, gbc);
        
        
        //INSTITUTION
        gbc.gridx = 0;
        gbc.gridy = 3;
        gbc.gridwidth = 1;
        JLabel label2 = new JLabel("INSTITUIÇÃO:");
        label2.setFont(font);
        label2.setForeground(Color.white);
        label2.setIcon(new ImageIcon("institution.png"));
        subPanel_1.add(label2, gbc);
        
        tfInstitution = new JTextField();
        tfInstitution.setBackground(new Color(242, 241, 239));
        tfInstitution.setFont(font);
        tfInstitution.setPreferredSize(new Dimension(400,25));
        gbc.gridx = 1;
        gbc.gridy = 3;
        gbc.gridwidth = 1;
        subPanel_1.add(tfInstitution, gbc);
        
        //CNPJ
        gbc.gridx = 0;
        gbc.gridy = 4;
        gbc.gridwidth = 1;
        JLabel label5 = new JLabel("CNPJ:");
        label5.setFont(font);
        label5.setForeground(Color.white);
        label5.setIcon(new ImageIcon("cnpj.png"));
        subPanel_1.add(label5, gbc);
        
        tfCnpj = new JTextField();
        tfCnpj.setBackground(new Color(242, 241, 239));
        tfCnpj.setFont(font);
        tfCnpj.setPreferredSize(new Dimension(400,25));
        gbc.gridx = 1;
        gbc.gridy = 4;
        gbc.gridwidth = 1;
        subPanel_1.add(tfCnpj, gbc);
        
        //FONE
        gbc.gridx = 0;
        gbc.gridy = 5;
        gbc.gridwidth = 1;
        JLabel label3 = new JLabel("FONE:");
        label3.setFont(font);
        label3.setForeground(Color.white);
        label3.setIcon(new ImageIcon("fone.png"));
        subPanel_1.add(label3, gbc);
        
        tfFone = new JTextField();
        tfFone.setBackground(new Color(242, 241, 239));
        tfFone.setFont(font);
        tfFone.setPreferredSize(new Dimension(400,25));
        gbc.gridx = 1;
        gbc.gridy = 5;
        gbc.gridwidth = 1;
        subPanel_1.add(tfFone, gbc);
        
        //EMAIL
        gbc.gridx = 0;
        gbc.gridy = 6;
        gbc.gridwidth = 1;
        JLabel label4 = new JLabel("EMAIL:");
        label4.setFont(font);
        label4.setForeground(Color.white);
        label4.setIcon(new ImageIcon("email.png"));
        subPanel_1.add(label4, gbc);
        
        tfEmail = new JTextField();
        tfEmail.setBackground(new Color(242, 241, 239));
        tfEmail.setFont(font);
        tfEmail.setPreferredSize(new Dimension(400,25));
        gbc.gridx = 1;
        gbc.gridy = 6;
        gbc.gridwidth = 1;
        subPanel_1.add(tfEmail, gbc);
        
        painelPrincipal.add(subPanel_1);
        
        subPanel_2 = new JPanel();
        subPanel_2.setLayout(new FlowLayout(FlowLayout.RIGHT,2,2));
        bReturn = new JButton("RETORNAR");
        bReturn.setIcon(new ImageIcon("return.png"));
        bReturn.setBackground(new java.awt.Color(218, 223, 225));
        subPanel_2.add(bReturn);
        bReturn.addActionListener(this);
        
        painelPrincipal.add(subPanel_2);
        
        subPanel_1.setBackground(new java.awt.Color(52, 73, 94));
        subPanel_2.setBackground(new java.awt.Color(52, 73, 94));
        
        frame.add(painelPrincipal);
        
        frame.pack();
        FormUtils.centerForm(frame);
        frame.setVisible(true);
        
        lerEntidades();
        exibirEntidade(getNumEntidadeCorrente());
    }
    
    public void actionPerformed(ActionEvent e){
        if(e.getSource().equals(bReturn)){
            bReturn_Click();
        }
    }
    
    private void bReturn_Click(){
        frame.setVisible(false);
        formPai.setEnabled(true);
        frame.dispose();
    }
    
    public Client getEntidadeCorrente(){
        Client client = new Client();
        int id;
        
        if(tfId.getText().isEmpty() == false){
            id = Integer.parseInt(tfId.getText());
            client.setId(id);
        }
        
        if(tfInstitution.getText().isEmpty() == false){
            client.setInstitution(tfInstitution.getText());
        }
        
        if(tfCnpj.getText().isEmpty() == false){
            client.setCnpj(tfCnpj.getText());
        }
        
        if(tfFone.getText().isEmpty() == false){
            client.setFone(tfFone.getText());
        }
        
        if(tfEmail.getText().isEmpty() == false){
            client.setEmail(tfEmail.getText());
        }
        return client;
    }
    
    public void exibirEntidade(int index){
        Client client;
        if(index == -1){
            FormUtils.clearTextFields(this,subPanel_1);
        }
        else{
            client = clientList.get(index);
            tfId.setText(Integer.toString(client.getId()));
            tfInstitution.setText(client.getInstitution());
            tfCnpj.setText(client.getCnpj());
            tfFone.setText(client.getFone());
            tfEmail.setText(client.getEmail());
        }
    }
    
    public void salvarEntidade(){
        Client client;
        
        client = getEntidadeCorrente();
        Client clientCreate = controller.create(client);
    }
    
    public void alterarEntidade(){
        Client client = new Client();
        
        client = getEntidadeCorrente();
        
        controller.update(client);
        
        lerEntidades();
        exibirEntidade(getNumEntidadeCorrente());
    }
    
    public void deletarEntidade(){
        Client client = new Client();
        
        client = getEntidadeCorrente();
        
        controller.delete(client);
        
        lerEntidades();
        exibirEntidade(getNumEntidadeCorrente());
    }
    
    public void lerEntidades(){
        clientList = controller.readAll();
        
        if(clientList.isEmpty()){
            setNumEntidadeCorrente(-1);
        }
        else{
            setNumEntidadeCorrente(0);
        }
    }
    
    public int totalEntidades(){
        return clientList.size();
    }
    
    public JPanel getSubPainelCampos(){
        return subPanel_1;
    }
 
}

package view;


/**
 * Escreva a descrição da classe DialogRelacionarClientLixeira aqui.
 * 
 * @author (seu nome) 
 * @version (número de versão ou data)
 */
import javax.swing.JFrame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JPanel;
import javax.swing.JList;
import java.util.List;
import javax.swing.DefaultListModel;
import javax.swing.JDialog;
import java.awt.GridLayout;
import javax.swing.JButton;
import java.awt.FlowLayout;
import java.awt.Dimension;
import java.util.ArrayList;
import javax.swing.JScrollPane;
import javax.swing.JLabel;

import model.entity.Lixeira;
import model.Database;
import controller.LixeiraController;

public class DialogRelacionarLixeiraClient extends JFrame implements ActionListener
{
    private static int ALTURA = 25;
    private static int LARGURA = 95;
    
    private FormClientLixeiras formPai;
    private String appNome;
    private LixeiraController controller;
    private JList<String> jListLixeiras;
    private JScrollPane barraRolagem;
    private List<Lixeira> lixeiras;
    private List<Lixeira> lixeirasSelecionadas;
    private JFrame frame;
    private JDialog jDialog;
    private JPanel painelBotoes;
    private JButton botaoCancelar;
    private JButton botaoOK;
    
    private int itensSelecionados [];
    
    public DialogRelacionarLixeiraClient(FormClientLixeiras f, Database database, String n){
        this.formPai = f;
        this.controller = new LixeiraController(database);
        this.lixeiras = controller.readAll();
        this.appNome = n;
    }
    
    public void criarExibirForm(){
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Dimension tamanhoBotao = new Dimension(this.LARGURA, this.ALTURA);
        
        DefaultListModel<String> l = new DefaultListModel<>();
        for(Lixeira li: lixeiras){
            l.addElement(li.getLoc());
        }
        
        jDialog = new JDialog(this, appNome, true);
        jDialog.setLayout(new GridLayout(3,1));
        
        jDialog.add(new JLabel("Selecione as lixeiras:"));
        
        jListLixeiras = new JList<>(l);
        barraRolagem = new JScrollPane(jListLixeiras);
        jDialog.add(barraRolagem);
        
        painelBotoes = new JPanel();
        painelBotoes.setLayout(new FlowLayout(FlowLayout.CENTER));
        botaoCancelar = new JButton("Cancelar");
        botaoCancelar.setPreferredSize(tamanhoBotao);
        botaoCancelar.addActionListener(this);
        painelBotoes.add(botaoCancelar);
        botaoOK = new JButton("OK");
        botaoOK.setPreferredSize(tamanhoBotao);
        botaoOK.addActionListener(this);
        painelBotoes.add(botaoOK);
        jDialog.add(painelBotoes);
        
        jDialog.setSize(250, 250);
        jDialog.setLocationRelativeTo(null);
        jDialog.setVisible(true);
        
        this.setVisible(true);
    }
    
    
    public void actionPerformed(ActionEvent e){
        if(e.getSource().equals(botaoCancelar)){
            botaoCancelar_Click();
        }
        
        if(e.getSource().equals(botaoOK)){
            botaoOK_Click();
        }
    }
    
    private void botaoCancelar_Click(){
        this.dispose();
    }
    
    private void botaoOK_Click(){
        lixeirasSelecionadas = new ArrayList<Lixeira>();
        itensSelecionados = jListLixeiras.getSelectedIndices();
        for(int i=0; i<itensSelecionados.length; i++){
            lixeirasSelecionadas.add(lixeiras.get(itensSelecionados[i]));
        }
        
        formPai.setLixeirasSelecionadas(lixeirasSelecionadas);
        this.dispose();
    }
}

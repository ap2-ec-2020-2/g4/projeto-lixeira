
package view;

import javax.swing.*;
import java.awt.*;
import java.util.List;
import java.awt.event.ActionEvent;

import view.ClientForm;
import model.Database;
import view.util.FormUtils;
import model.entity.Client;
import controller.ClientController;

import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 * Escreva a descrição da classe SendEmailForm aqui.
 * 
 * @author (seu nome) 
 * @version (número de versão ou data)
 */
public class SendEmailForm extends FormEntidade
{
    private JFrame frame;
    private JFrame formPai;
    private Database database;
    
    String emailAtual;
    
    private JPanel painelPrincipal;
    private JPanel subPainel_1;
    private JPanel subPainel_2;
    private JPanel subPainel_3;
    private JTextField tfIdClient;
    private JTextField tfInstitutionClient;
    private JTextField tfFoneClient;
    private JTextField tfEmailClient;
    private JTextField tfCnpjClient;
    private JTextField tfEnviarEmail;
    
    private JButton botaoReturn;
    private JButton botaoPrevious;
    private JButton botaoNext;
    private JButton botaoEnviarEmail;
    
    private List<Client> clientList;
    private ClientController controllerClient;
    
    public SendEmailForm(JFrame form, Database db, String n){
        formPai = form;
        this.database = db;
        this.controllerClient = new ClientController(database);
        setAppNome(n);
    }
    
    public void criarExibirForm(){
        lerClients();
        
        frame = new JFrame(getAppNome());
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        Font font = new Font("Berlin Sans", Font.BOLD, 18);
        
        painelPrincipal = new JPanel();
        painelPrincipal.setLayout(new GridLayout(2,1));
        
        subPainel_1 = new JPanel();
        subPainel_1.setBackground(new java.awt.Color(52, 73, 94));
        subPainel_1.setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = new Insets(3,3,3,3);
        
        botaoPrevious = new JButton("PROXIMO");
        botaoPrevious.setFont(font);
        gbc.gridx = 1;
        gbc.gridy = 0;
        botaoPrevious.setIcon(new ImageIcon("next.png"));
        botaoPrevious.setBackground(new java.awt.Color(218, 223, 225));
        botaoNext = new JButton("ANTERIOR");
        botaoNext.setFont(font);
        gbc.gridx = 2;
        gbc.gridy = 0;
        botaoNext.setIcon(new ImageIcon("previous.png"));
        botaoNext.setBackground(new java.awt.Color(218, 223, 225));
        botaoPrevious.setPreferredSize(new Dimension(200, 45));
        botaoNext.setPreferredSize(new Dimension(200, 45));
        botaoPrevious.addActionListener(this);
        botaoNext.addActionListener(this);
        subPainel_1.add(botaoNext);
        subPainel_1.add(botaoPrevious);
        painelPrincipal.add(subPainel_1);
        
        JLabel label8 = new JLabel("CLIENTES:");
        label8.setFont(font);
        label8.setForeground(Color.white);
        label8.setIcon(new ImageIcon("cliente.png"));
        gbc.gridx = 0;
        gbc.gridy = 2;
        gbc.gridwidth = 7;
        subPainel_1.add(label8, gbc);
        
        JLabel label9 = new JLabel(" ");
        gbc.gridx = 0;
        gbc.gridy = 8;
        gbc.gridwidth = 7;
        subPainel_1.add(label9, gbc);
        
        //ID
        gbc.gridx = 0;
        gbc.gridy = 3;
        gbc.gridwidth = 1;
        JLabel label1 = new JLabel("ID:");
        label1.setFont(font);
        label1.setForeground(Color.white);
        label1.setIcon(new ImageIcon("id.png"));
        subPainel_1.add(label1, gbc);
        tfIdClient = new JTextField();
        tfIdClient.setFont(font);
        tfIdClient.setPreferredSize(new Dimension(70,25));
        tfIdClient.setBackground(new Color(242, 241, 239));
        tfIdClient.setEnabled(false);
        gbc.gridx = 1;
        gbc.gridy = 3;
        gbc.gridwidth = 1;
        subPainel_1.add(tfIdClient,gbc);
        
        
        //INSTITUTION
        gbc.gridx = 0;
        gbc.gridy = 4;
        gbc.gridwidth = 1;
        JLabel label2 = new JLabel("INSTITUIÇÃO:");
        label2.setFont(font);
        label2.setForeground(Color.white);
        label2.setIcon(new ImageIcon("institution.png"));
        subPainel_1.add(label2, gbc);
        tfInstitutionClient = new JTextField();
        tfInstitutionClient.setFont(font);
        tfInstitutionClient.setPreferredSize(new Dimension(400,25));
        tfInstitutionClient.setBackground(new Color(242, 241, 239));
        tfInstitutionClient.setEnabled(false);
        gbc.gridx = 1;
        gbc.gridy = 4;
        gbc.gridwidth = 2;
        subPainel_1.add(tfInstitutionClient,gbc);
        
        //CNPJ
        gbc.gridx = 0;
        gbc.gridy = 5;
        gbc.gridwidth = 1;
        JLabel label5 = new JLabel("CNPJ:");
        label5.setFont(font);
        label5.setForeground(Color.white);
        label5.setIcon(new ImageIcon("cnpj.png"));
        subPainel_1.add(label5, gbc);
        tfCnpjClient = new JTextField();
        tfCnpjClient.setFont(font);
        tfCnpjClient.setPreferredSize(new Dimension(400,25));
        tfCnpjClient.setBackground(new Color(242, 241, 239));
        tfCnpjClient.setEnabled(false);
        gbc.gridx = 1;
        gbc.gridy = 5;
        gbc.gridwidth = 2;
        subPainel_1.add(tfCnpjClient,gbc);
        
        //FONE
        gbc.gridx = 0;
        gbc.gridy = 6;
        gbc.gridwidth = 1;
        JLabel label3 = new JLabel("FONE:");
        label3.setFont(font);
        label3.setForeground(Color.white);
        label3.setIcon(new ImageIcon("fone.png"));
        subPainel_1.add(label3, gbc);
        tfFoneClient = new JTextField();
        tfFoneClient.setFont(font);
        tfFoneClient.setPreferredSize(new Dimension(400,25));
        tfFoneClient.setBackground(new Color(242, 241, 239));
        tfFoneClient.setEnabled(false);
        gbc.gridx = 1;
        gbc.gridy = 6;
        gbc.gridwidth = 2;
        subPainel_1.add(tfFoneClient,gbc);
        
        //EMAIL
        gbc.gridx = 0;
        gbc.gridy = 7;
        gbc.gridwidth = 1;
        JLabel label4 = new JLabel("EMAIL:");
        label4.setFont(font);
        label4.setForeground(Color.white);
        label4.setIcon(new ImageIcon("email.png"));
        subPainel_1.add(label4, gbc);
        tfEmailClient = new JTextField();
        tfEmailClient.setFont(font);
        tfEmailClient.setPreferredSize(new Dimension(400,25));
        tfEmailClient.setBackground(new Color(242, 241, 239));
        tfEmailClient.setEnabled(false);
        gbc.gridx = 1;
        gbc.gridy = 7;
        gbc.gridwidth = 2;
        subPainel_1.add(tfEmailClient,gbc);
        
        painelPrincipal.add(subPainel_1);
        
        subPainel_3 = new JPanel();
        subPainel_3.setBackground(new java.awt.Color(52, 73, 94));
        subPainel_3.setLayout(new GridBagLayout());
        GridBagConstraints gbc1 = new GridBagConstraints();
        gbc1.fill = GridBagConstraints.HORIZONTAL;
        gbc1.insets = new Insets(3,3,3,3);
        
        tfEnviarEmail = new JTextField();
        tfEnviarEmail.setFont(font);
        tfEnviarEmail.setPreferredSize(new Dimension(400,25));
        tfEnviarEmail.setBackground(new Color(242, 241, 239));
        tfEnviarEmail.setEnabled(false);
        gbc1.gridx = 0;
        gbc1.gridy = 1;
        gbc1.gridwidth = 2;
        subPainel_3.add(tfEnviarEmail,gbc1);
        
        botaoEnviarEmail = new JButton("ENVIAR EMAIL");
        botaoEnviarEmail.setFont(font);
        gbc1.gridx = 2;
        gbc1.gridy = 1;
        botaoEnviarEmail.setIcon(new ImageIcon("send_mail.png"));
        botaoEnviarEmail.setBackground(new java.awt.Color(218, 223, 225));
        botaoEnviarEmail.setPreferredSize(new Dimension(200, 45));
        botaoEnviarEmail.addActionListener(this);
        subPainel_3.add(botaoEnviarEmail, gbc1);
        
        botaoReturn = new JButton("RETORNAR");
        botaoReturn.setIcon(new ImageIcon("return.png"));
        botaoReturn.setPreferredSize(new Dimension(120, 50));
        botaoReturn.setBackground(new java.awt.Color(218, 223, 225));
        gbc1.gridx = 3;
        gbc1.gridy = 4;
        subPainel_3.add(botaoReturn, gbc1);
        botaoReturn.addActionListener(this);
        
        JLabel label11 = new JLabel(" ");
        gbc1.gridx = 0;
        gbc1.gridy = 3;
        gbc1.gridwidth = 7;
        subPainel_3.add(label11, gbc1);
        
        
        
        painelPrincipal.add(subPainel_3);
        
        painelPrincipal.setPreferredSize(new Dimension(1000,600));
        frame.add(painelPrincipal);
        frame.pack();
        FormUtils.centerForm(frame);        
        frame.setVisible(true);

        exibirEntidade(getNumEntidadeCorrente());
    }
    
    public void actionPerformed(ActionEvent e){
        if(e.getSource().equals(botaoPrevious)){
            botaoPrevious_Click();
        }
        
        if(e.getSource().equals(botaoNext)){
            botaoNext_Click();
        }
        
        if(e.getSource().equals(botaoEnviarEmail)){
            botaoEnviarEmail_Click();
        }
        
        if(e.getSource().equals(botaoReturn)){
            botaoReturn_Click();
        }
    }
    
    public Client getClientCorrente(){
        Client c = clientList.get(getNumEntidadeCorrente());
        
        return c;
    }
    
    public void salvarEntidade(){
    }
    
    public void alterarEntidade(){
    }
    
    public void deletarEntidade(){
    }
    
    public void lerClients(){
        clientList = controllerClient.readAll();
        
        if(clientList.isEmpty()){
            setNumEntidadeCorrente(-1);
        }
        else{
            setNumEntidadeCorrente(0);
        }
    }
    
    public void lerEntidades(){
    }
    
    public void exibirEntidade(int index){
        Client client;
        
        if(index == -1){
            FormUtils.clearTextFields(this,subPainel_1);
        }
        else{
            client = clientList.get(index);
            tfIdClient.setText(Integer.toString(client.getId()));
            tfInstitutionClient.setText(client.getInstitution());
            tfFoneClient.setText(client.getFone());
            tfCnpjClient.setText(client.getCnpj());
            tfEmailClient.setText(client.getEmail());
            tfEnviarEmail.setText(client.getEmail());
            
            emailAtual = tfEnviarEmail.getText();
            
        }
        
    }
    
    public int totalEntidades(){
        return clientList.size();
    }
    
    public JPanel getSubPainelCampos(){
        return subPainel_1;
    }
    
    private void botaoNext_Click(){
        int numEntidadeCorrente = getNumEntidadeCorrente();
        
        if((numEntidadeCorrente-1) >= 0){
            numEntidadeCorrente--;
            setNumEntidadeCorrente(numEntidadeCorrente);
            exibirEntidade(numEntidadeCorrente);
        }
    }
    
    private void botaoPrevious_Click(){
        int numEntidadeCorrente = getNumEntidadeCorrente();
        
        if((numEntidadeCorrente+1) < totalEntidades()){
            numEntidadeCorrente++;
            setNumEntidadeCorrente(numEntidadeCorrente);
            exibirEntidade(numEntidadeCorrente);
        }
    }
    
    private void botaoReturn_Click(){
        frame.setVisible(false);
        formPai.setEnabled(true);
        frame.dispose();
    }
    
    private void botaoEnviarEmail_Click(){
        Properties props = new Properties();
            /** Parâmetros de conexão com servidor Hotmail */
            props.put("mail.transport.protocol", "smtp");
            props.put("mail.smtp.host", "smtp.live.com");
            props.put("mail.smtp.socketFactory.port", "587");
            props.put("mail.smtp.socketFactory.fallback", "false");
            props.put("mail.smtp.starttls.enable", "true");
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.port", "587");

            Session session = Session.getDefaultInstance(props,
                        new javax.mail.Authenticator() {
                             @Override
                             protected PasswordAuthentication getPasswordAuthentication()
                             {
                                   return new PasswordAuthentication("projetolixeira@outlook.com", "lixeiraap2");
                             }
                        });

            /** Ativa Debug para sessão */
            session.setDebug(true);

            try {

                  Message message = new MimeMessage(session);
                  message.setFrom(new InternetAddress("projetolixeira@outlook.com")); //Remetente

                  message.setRecipients(Message.RecipientType.TO,
                                    InternetAddress.parse(emailAtual)); //Destinatário(s)
                  message.setSubject("Situação Lixeira");//Assunto
                  message.setText("UMA DE SUAS LIXEIRAS SE ENCONTRA CHEIA\nFAVOR FAZER A COLETA!!!");
                  /**Método para enviar a mensagem criada*/
                  Transport.send(message);

                  System.out.println("Feito!!!");

             } catch (MessagingException e) {
                  throw new RuntimeException(e);
            }
      }
}

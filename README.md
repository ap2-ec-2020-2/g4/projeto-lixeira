# Sistema para controle de lixeiras automáticas 

### O projeto tem o objetivo de atender preferencialmente prefeituras que façam a aquisição das lixeiras com fechamento automático/controle de lotação, visando o registro das mesmas, sua localização e a atualização de seu estado atual (cheia/vazia), essa última através de um conexão com arduíno, tecnologia usada nas lixeiras. 

# Utilização
* Ao instalar o Java na máquina e as bibliotecas encontradas na pasta "libs", o usuário estará pronto para rodar o sistema, além disso, é bom ler a parte de "Observações" encontrada mais abaixo neste readme para mais recomendações. Ao abri-lo, o usuário visualizará a página incial da aplicação, onde no canto superior esquerdo poderá escolher no menu qual tela deseja abrir.

### Menu Clientes
Na tela clientes é possível fazer:
* O cadastro do cliente, clicando no primeiro botão do painel de botões e preenchendo os dados. São eles, o nome da instituição, o CNPJ da empresa, o telefone e o email real da mesma. Logo depois, apertando no ícone de confirmar o cadastro estará completo e um ID será vinculado ao cliente recém criado.
* Navegação pelos clientes criados com os botões de navegação, são eles o segundo e terceiro do painel de botões.
* A edição dos dados dos clientes, navegando até ele com os botões de navegação, fazendo as mudanças necessárias nos campos de texto e clicando em seguida no quarto botão do painel, com o ícone de edição, e aceitando a confirmação seguinte.
* A exclusão de um cliente, navegando até ele com os botões de navegação, e clicando no quinto botão, com ícone de lixeira, e confirmando a mensagem que aparecerá na tela.
* Confirmação e cancelamento de ações, clicando com os dois últimos botões quando possível para cancelar ou confimar uma ação, como foi dito anteriormente.
* Retorno ao menu inicial, clicanco no botão retornar, abaixo dos campos de texto.

### Menu Lixeiras
Na tela Lixeiras é possível fazer:
* O cadastro de uma lixeira, clicando no primeiro botão do painel de botões e preenchendo a localização da mesma, com um endereço correto. Logo depois, apertando no ícone de confirmar o cadastro estará completo e um ID será vinculado a lixeira recém criada.
* Navegação pelas lixeiras criadas com os botões de navegação, são eles o segundo e terceiro do painel de botões.
* A edição dos dados das lixeiras, navegando até ele com os botões de navegação, fazendo as mudanças necessárias nos campos de texto e clicando em seguida no quarto botão do painel, com o ícone de edição, e aceitando a confirmação seguinte.
* A exclusão de uma lixeira, navegando até ele com os botões de navegação, e clicando no quinto botão, com ícone de lixeira, e confirmando a mensagem que aparecerá na tela.
* Confirmação e cancelamento ações, clicando com os dois últimos botões quando possível para cancelar ou confimar uma ação, como foi dito anteriormente.
* Retorno ao menu inicial, clicanco no botão "retornar", abaixo dos campos de texto.
* Abertura do Lixeira's Map, ferramenta para localização do endereço da lixeira desejada, clicanco no botão "ir para o maps", abaixo dos campos de texto. Dentro dessa ferramenta, no campo de texto é possível pesquisar os endereços clicando no botão "Search", uma marcação do local aparecerá no mapa. A ferramenta street view também pode ser usada levando o ícone de um boneco, no canto inferior direito da tela até o mapa. Além disso, clicando no botão "recarregar" o mapa é recarregado para o posição inicial e o botão "retornar" fecha a ferramenta maps e volta ao formulário das lixeiras.
* Abertura da ferramenta de monitoramento em nuvem (thinkspeak), clicando no botão "status", será aberta uma página web, onde é possível verificar a situação das lixeiras (a distância entre a tampa e o volume de lixo).
* Retorno ao menu inicial, clicanco no botão "retornar", abaixo da tabela de lixeiras.

### Menu Cliente-Lixeira
Na tela Cliente-Lixeira é possível fazer:

* Relacionamento entre as lixeiras existentes com os clientes registrados, clicando no botão "relacionar" e selecionando a(s) lixeira(s) que estão associadas aquele cliente.
* Navegação pelos clientes e as lixeiras vinculadas a eles, com os botões de navegação, são eles o segundo e terceiro do painel de botões.
* Retorno ao menu inicial, clicanco no botão "retornar", abaixo dos campos de texto.

### Menu Enviar email
Na tela Enviar Email é possível fazer:

* Navegação pelos clientes registrados, com os botões de navegação, os únicos do painel superior.
* Envio de um email solicitando a coleta de uma lixeira que se encontra cheia, clicando no botão "Enviar Email", antes, o usuário deve certificar-se que o email para o qual ele deseja enviar a mensagem é o que pode ser visuliazado no campo de texto ao lado do botão.
* Retorno ao menu inicial, clicanco no botão "retornar", abaixo dos campos de texto.

# Observações
* O caminho do arquivo HTML "simple map", que se encontra na pasta "HTML Gmaps", deve ser corretamente colocado no código da classe "Menu" que se encontra dentro do pacote "view" da aplicação.
* Para o funcionamento do envio do email, pode ser necessária, a desativação temporária do antívirus instalado na máquina, por questões de erro.

## Os pontos inicialmente previstos pelo projeto, mas que não foram utilizados/atingidos por limitações são:
* Registro mais eficiente do status das lixeiras, dificultado, por usarmos um simulador arduíno (tinkercad), que permite o envio de dados para a nuvem através de um placa (ESP 8266), mas não permite um detalhamente melhor para "cada" lixeira e sua situação atual, por ser apenas um simulador.
* O salvamento das marcações feitas no mapa. A API utilizada para a criação do Lixeira's Map permite métodos para salvar as marcações, porém, foi achado apenas em códigos PHP que não conseguimos converter para o JAVA. Além disso, requer conhecimento mais específico de banco de dados para manipular as marcações salvas. Logo foi aplicado apenas a função para pesquisar os locais.

# Construído com
* https://netbeans.apache.org/download/nb123/nb123.html - Netbeans
* https://www.bluej.org/ - Bluej
* https://developers.google.com/maps/documentation/javascript/examples/map-simple?hl=pt-br - API Google Maps JavaScript
* https://thingspeak.com - ThinkSpeak (Armazanamento dos dados arduíno na nuvem)
* https://www.tinkercad.com/dashboard - Tinkercad (Simulador Arduíno)

##### VERSÃO: 2021.1

# AUTORES:

* Bernardo Halfeld de Assis Nébias @bernardohalfeld (Líder)
* Flávio Jerônimo da Silva Filho @flaviojeronimo
* Gustavo Santos da Silva @gustavo.santos042
* Paulo Carneiro FIlho @Paul0c
* Yugo Oliveira Montalvão @yugo.oliveira



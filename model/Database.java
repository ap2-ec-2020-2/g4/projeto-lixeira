package model;


/**
 * Escreva a descrição da classe Database aqui.
 * 
 * @author (seu nome) 
 * @version (número de versão ou data)
 */
import java.sql.*;
import com.j256.ormlite.jdbc.JdbcConnectionSource;

public class Database
{
    private String databaseName = null;
    private JdbcConnectionSource connection = null;
    
    public Database(String databaseName){
        this.databaseName = databaseName;
    }
    
    public JdbcConnectionSource getConnection() throws SQLException{
        if(connection == null){
            try{
                connection = new JdbcConnectionSource("jdbc:sqlite:"+databaseName);
            }catch(Exception e){
                System.err.println(e.getClass().getName()+ ": " + e.getMessage() );
                System.exit(0);
            }
            System.out.println("Opened database sucessfully");
        }
        return connection;
    }
    
    public void close(){
        if(connection != null){
            try{
                connection.close();
                this.connection = null;
            }catch(java.io.IOException e){
                System.err.println(e);
            }
        }
    }
            
}

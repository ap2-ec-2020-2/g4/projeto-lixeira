package model;


/**
 * Escreva a descrição da classe ClientDao aqui.
 * 
 * @author (seu nome) 
 * @version (número de versão ou data)
 */
import model.entity.Client;

import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.dao.Dao;
import java.util.List;
import java.util.ArrayList;
import java.sql.SQLException;
import com.j256.ormlite.table.TableUtils;

public class ClientDao
{
    private static Database database;
    private static Dao<Client, Integer> dao;
    private List<Client> loadedClients;
    private Client loadedClient;
    
    public ClientDao(Database database){
        ClientDao.setDatabase(database);
        loadedClients = new ArrayList<Client>();
    }
    
    public static void setDatabase(Database database){
        ClientDao.database = database;
        try{
            dao = DaoManager.createDao(database.getConnection(), Client.class);
            TableUtils.createTableIfNotExists(database.getConnection(), Client.class);
        }catch(SQLException e){
            System.err.println(e);
        }
    }
    
    public Client create(Client client){
        int nrows = 0;
        try{
            nrows = dao.create(client);
            if(nrows == 0){
                throw new SQLException("Error: object not saved");
            }
            this.loadedClient = client;
            loadedClients.add(client);
        }catch(SQLException e){
            System.err.println(e);
        }
        return client;
    }
    
    public Client ReadFromId(int id){
        try{
            this.loadedClient = dao.queryForId(id);
            if(this.loadedClient != null){
                this.loadedClients.add(this.loadedClient);
            }
        }catch(SQLException e){
            System.err.println(e);
        }
        return this.loadedClient;
    }
    
    public List<Client> readAll(){
        try{
            this.loadedClients = dao.queryForAll();
            if(this.loadedClients.size() != 0){
                this.loadedClient = this.loadedClients.get(0);
            }
        }catch(SQLException e){
            System.err.println(e);
        }
        return this.loadedClients;
    }
    
    public void update(Client client){
        try{
            dao.update(client);
        }catch(SQLException e){
            System.err.println(e);
        }
    }
    
    public void delete(Client client){
        try{
            dao.deleteById(client.getId());
        }catch(SQLException e){
            System.err.println(e);
        }
    }       
}

package model;

/**
 * Escreva a descrição da classe LixeiraDao aqui.
 * 
 * @author (seu nome) 
 * @version (número de versão ou data)
 */
import model.entity.Lixeira;

import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.dao.Dao;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;
import com.j256.ormlite.table.TableUtils;

public class LixeiraDao
{
    private static Database database;
    private static Dao<Lixeira, Integer> dao;
    private List<Lixeira> loadedLixeiras;
    private Lixeira loadedLixeira;
    
    public LixeiraDao(Database database){
       LixeiraDao.setDatabase(database);
       loadedLixeiras = new ArrayList<Lixeira>();
    }
    
    public static void setDatabase(Database database){
        LixeiraDao.database = database;
        try{
            dao = DaoManager.createDao(database.getConnection(), Lixeira.class);
            TableUtils.createTableIfNotExists(database.getConnection(), Lixeira.class);
        }catch(SQLException e){
            System.err.println(e);
        }
    }
    
    public Lixeira create(Lixeira lixeira){
        int nrows = 0;
        try{
            nrows =dao.create(lixeira);
            if(nrows == 0){
                throw new SQLException("Error: object not saved");
            }
            this.loadedLixeira = lixeira;
            loadedLixeiras.add(lixeira);
        }catch(SQLException e){
            System.err.println(e);
        }
        return lixeira;
    }
    
    public Lixeira readFromId(int id){
        try{
            this.loadedLixeira = dao.queryForId(id);
            if(this.loadedLixeira != null){
                this.loadedLixeiras.add(this.loadedLixeira);
            }
        }catch(SQLException e){
            System.err.println(e);
        }
        return this.loadedLixeira;
    }
    
    public List<Lixeira> readAll(){
        try{
            this.loadedLixeiras = dao.queryForAll();
            if(this.loadedLixeiras.size() != 0){
                this.loadedLixeira = this.loadedLixeiras.get(0);
            }
        }catch(SQLException e){
            System.err.println(e);
        }
        return this.loadedLixeiras;
    }
    
    public void update(Lixeira lixeira){
        try{
            dao.update(lixeira);
        }catch(java.sql.SQLException e){
            System.err.println(e);
        }
    }
    
    public void delete(Lixeira lixeira){
        try{
            dao.deleteById(lixeira.getId());
        }catch(SQLException e){
            System.err.println(e);
        }
    }
       
}

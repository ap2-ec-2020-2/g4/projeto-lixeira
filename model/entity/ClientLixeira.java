package model.entity;


/**
 * Escreva a descrição da classe ClientLixeira aqui.
 * 
 * @author (seu nome) 
 * @version (número de versão ou data)
 */
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.j256.ormlite.field.DataType;

public class ClientLixeira
{
    @DatabaseField(generatedId = true)
    private int id;
    
    @DatabaseField(canBeNull = false, foreign = true)
    private Lixeira lixeira;
    
    @DatabaseField(canBeNull = false, foreign = true)
    private Client client;
    
    public void ClientLixeira(Client c, Lixeira l){
        client = c;
        lixeira = l;
    }
    
    public void setId(int i){
        id = i;
    }
    
    public void setLixeira(Lixeira l){
        lixeira = l;
    }
    
    public void setClient(Client c){
        client = c;
    }
    
    public int getId(){
        return id;
    }
    
    public Client getClient(){
        return client;
    }
    
    public Lixeira getLixeira(){
        return lixeira;
    }
}

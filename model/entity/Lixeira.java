package model.entity;


/**
 * Escreva a descrição da classe Lixeira aqui.
 * 
 * @author (seu nome) 
 * @version (número de versão ou data)
 */

import com.j256.ormlite.table.DatabaseTable;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.DataType;

@DatabaseTable(tableName = "Lixeira")

public class Lixeira
{
    @DatabaseField(generatedId = true)
    private int id;
    
    @DatabaseField
    private String loc;

    //Start GetterSetterExtension Source Code
    /**GET Method Propertie id*/
    public int getId(){
        return this.id;
    }//end method getId

    /**SET Method Propertie id*/
    public void setId(int id){
        this.id = id;
    }//end method setId

    /**GET Method Propertie loc*/
    public String getLoc(){
        return this.loc;
    }//end method getLoc

    /**SET Method Propertie loc*/
    public void setLoc(String loc){
        this.loc = loc;
    }//end method setLoc

    //End GetterSetterExtension Source Code
//!
}

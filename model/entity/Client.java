package model.entity;

/**
 * Escreva a descrição da classe Client aqui.
 * 
 * @author (seu nome) 
 * @version (número de versão ou data)
 */

import com.j256.ormlite.table.DatabaseTable;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.DataType;

@DatabaseTable(tableName = "Client")

public class Client
{
    @DatabaseField(generatedId = true)
    private int id;
    
    @DatabaseField
    private String Institution;
    
    @DatabaseField
    private String Cnpj;
    
    @DatabaseField
    private String Fone;
    
    @DatabaseField
    private String Email;
    

    //Start GetterSetterExtension Source Code
    /**GET Method Propertie id*/
    public int getId(){
        return this.id;
    }//end method getId

    /**SET Method Propertie id*/
    public void setId(int id){
        this.id = id;
    }//end method setId

    /**GET Method Propertie Institution*/
    public String getInstitution(){
        return this.Institution;
    }//end method getInstitution

    /**SET Method Propertie Institution*/
    public void setInstitution(String Institution){
        this.Institution = Institution;
    }//end method setInstitution

    /**GET Method Propertie Cnpj*/
    public String getCnpj(){
        return this.Cnpj;
    }//end method getCnpj

    /**SET Method Propertie Cnpj*/
    public void setCnpj(String Cnpj){
        this.Cnpj = Cnpj;
    }//end method setCnpj

    /**GET Method Propertie Fone*/
    public String getFone(){
        return this.Fone;
    }//end method getFone

    /**SET Method Propertie Fone*/
    public void setFone(String Fone){
        this.Fone = Fone;
    }//end method setFone

    /**GET Method Propertie Email*/
    public String getEmail(){
        return this.Email;
    }//end method getEmail

    /**SET Method Propertie Email*/
    public void setEmail(String Email){
        this.Email = Email;
    }//end method setEmail

    //End GetterSetterExtension Source Code
//!
}

package model;


/**
 * Escreva a descrição da classe ClientLixeiraDao aqui.
 * 
 * @author (seu nome) 
 * @version (número de versão ou data)
 */
import com.j256.ormlite.stmt.*;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.dao.Dao;
import java.sql.SQLException;
import com.j256.ormlite.table.TableUtils;
import java.util.*;

import model.entity.ClientLixeira;
import model.entity.Lixeira;
import model.entity.Client;
import model.LixeiraDao;

public class ClientLixeiraDao
{
    private static Database database;
    private static Dao<ClientLixeira, Integer> dao;
    private static LixeiraDao daoLixeira;
    private List<ClientLixeira> itensCarregados;
    private ClientLixeira itemCarregado;
    
    public ClientLixeiraDao(Database database){
        this.setDatabase(database);
        itensCarregados = new ArrayList<ClientLixeira>();
    }
    
    public static void setDatabase(Database database){
        ClientLixeiraDao.database = database;
        try{
            dao = DaoManager.createDao(database.getConnection(), ClientLixeira.class);
            TableUtils.createTableIfNotExists(database.getConnection(), ClientLixeira.class);
            
            daoLixeira = new LixeiraDao(database);
        }catch(SQLException e){
            System.out.println(e);
        }
    }
    
    public ClientLixeira create(ClientLixeira clientLixeira){
        int nrows = 0;
        try{
            nrows = dao.create(clientLixeira);
            if(nrows == 0){
                throw new SQLException("Erro ao salvar o objeto.");
            }
            this.itemCarregado = clientLixeira;
            itensCarregados.add(clientLixeira);
        }catch(SQLException e){
            System.out.println(e);
        }
        return clientLixeira;
    }
    
    public ClientLixeira readFromId(int id){
        try{
            this.itemCarregado = dao.queryForId(id);
            if(this.itemCarregado != null){
                this.itensCarregados.add(this.itemCarregado);
            }
        }catch(SQLException e){
                System.out.println(e);
        }
        return this.itemCarregado;
    }
    
    public List<ClientLixeira> readAll(){
        try{
            this.itensCarregados = dao.queryForAll();
            if(this.itensCarregados.size() != 0){
                this.itemCarregado = this.itensCarregados.get(0);
            }
        }catch(SQLException e){
            System.out.println(e);
        }
        return this.itensCarregados;
    }
    
    public List<Lixeira> readLixeirasDeClient(int idClient){
        int cod_lixeira;
        List<ClientLixeira> listaClientLixeiras = new ArrayList<ClientLixeira>();
        List<Lixeira> lixeiras = new ArrayList<Lixeira>();
        try{
            QueryBuilder<ClientLixeira, Integer> queryBuilder = dao.queryBuilder();
            queryBuilder.where().eq("client_id",idClient);
            listaClientLixeiras = dao.query(queryBuilder.prepare());
            
            for(ClientLixeira cl: listaClientLixeiras){
                cod_lixeira = cl.getLixeira().getId();
                
                lixeiras.add(daoLixeira.readFromId(cod_lixeira));
            }
        }catch(SQLException e){
            System.out.println(e);
        }
        return lixeiras;
    }
    
    public void delete(ClientLixeira clientLixeira){
        try{
            dao.deleteById(clientLixeira.getId());
        }catch(SQLException e){
            System.out.println(e);
        }
    }
    
    public void update(ClientLixeira clientLixeira){
        try{
            dao.update(clientLixeira);
        }catch(java.sql.SQLException e){
            System.out.println(e);
        }
    }
}

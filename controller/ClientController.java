package controller;


/**
 * Escreva a descrição da classe ClientController aqui.
 * 
 * @author (seu nome) 
 * @version (número de versão ou data)
 */
import java.util.List;
import java.util.ArrayList;

import model.entity.Client;
import model.Database;
import model.ClientDao;

public class ClientController
{
    private ClientDao dao;
    
    public ClientController(Database database){
        this.dao = new ClientDao(database);
    }
    
    public Client create(Client client){
        return dao.create(client);
    }
    
    public List<Client> readAll(){
        return dao.readAll();
    }
    
    public void update(Client client){
        dao.update(client);
    }
    
    public void delete(Client client){
        dao.delete(client);
    }
}

package controller;


/**
 * Escreva a descrição da classe ClientLixeiraController aqui.
 * 
 * @author (seu nome) 
 * @version (número de versão ou data)
 */

import java.util.*;

import model.LixeiraDao;
import model.entity.Lixeira;
import model.entity.ClientLixeira;
import model.ClientLixeiraDao;
import model.Database;

public class ClientLixeiraController
{
    private ClientLixeiraDao dao;
    private LixeiraDao daoLixeira;
    
    public ClientLixeiraController(Database database){
        this.dao = new ClientLixeiraDao(database);
    }
    
    public ClientLixeira create(ClientLixeira item){
        return dao.create(item);
    }
    
    public List<ClientLixeira> readAll(){
        return dao.readAll();
    }
    
    public List<Lixeira> readLixeiraDeClient(int idClient){
        return dao.readLixeirasDeClient(idClient);
    }
    
    public void update(ClientLixeira item){
        dao.update(item);
    }
    
    public void delete(ClientLixeira item){
        dao.delete(item);
    }
}

package controller;


/**
 * Escreva a descrição da classe LixeiraController aqui.
 * 
 * @author (seu nome) 
 * @version (número de versão ou data)
 */
import java.util.List;
import java.util.ArrayList;

import model.entity.Lixeira;
import model.Database;
import model.LixeiraDao;

public class LixeiraController
{
    private LixeiraDao dao;
    
    public LixeiraController(Database database){
        this.dao = new LixeiraDao(database);
    }
    
    public Lixeira create(Lixeira lixeira){
        return dao.create(lixeira);
    }
    
    public List<Lixeira> readAll(){
        return dao.readAll();
    }
    
    public void update(Lixeira lixeira){
        dao.update(lixeira);
    }
    
    public void delete(Lixeira lixeira){
        dao.delete(lixeira);
    }
}
